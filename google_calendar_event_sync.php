<?php
// Include Google calendar api handler class 
include_once 'GoogleCalendarApi.class.php';

// Include database configuration file 
require_once 'config.php';

if (isset($_GET['code'])) {
    // Initialize Google Calendar API class 
    $GoogleCalendarApi = new GoogleCalendarApi();

    // Get event ID from session 
    $eventData = $_SESSION['postData'];
    if (!empty($eventData)) {
        $calendar_event = array(
            'summary' => $eventData['title'],
            'location' => $eventData['location'],
            'description' => $eventData['description']
        );

        $event_datetime = array(
            'event_date' => $eventData['date'],
            'start_time' => $eventData['time_from'],
            'end_time' => $eventData['time_to']
        );

        // Get the access token 
        $data = $GoogleCalendarApi->GetAccessToken(GOOGLE_CLIENT_ID, REDIRECT_URI, GOOGLE_CLIENT_SECRET, $_GET['code']);
        $access_token = $data['access_token'];

        if (!empty($access_token)) {
            try {
                // Get the user's calendar timezone 
                $user_timezone = $GoogleCalendarApi->GetUserCalendarTimezone($access_token);

                // Create an event on the primary calendar 
                $google_event_id = $GoogleCalendarApi->CreateCalendarEvent($access_token, 'primary', $calendar_event, 0, $event_datetime, $user_timezone);
            } catch (Exception $e) {
                //header('Bad Request', true, 400); 
                echo json_encode(array('error' => 1, 'message' => $e->getMessage()));
                die();
                $statusMsg = $e->getMessage();
            }
        } else {
            $statusMsg = 'Failed to fetch access token!';
        }
    } else {
        $statusMsg = 'Event data not found!';
    }
    $_SESSION['status_response'] = array('status' => $status, 'status_msg' => $statusMsg);

    header("Location: index.php");
    exit();
}
