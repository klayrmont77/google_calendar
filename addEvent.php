<?php
// Include database configuration file 
require_once 'config.php';

$postData = $statusMsg = $valErr = '';
$status = 'danger';

// If the form is submitted 
if (isset($_POST['submit'])) {
    // Get event info 
    $_POST['attendees'] = 'clairmont.rajaonarison@gmail.con';
    $_SESSION['postData'] = $_POST;


    header("Location: $googleOauthURL");
    exit();
} else {
    $statusMsg = 'Form submission failed!';
}

$_SESSION['status_response'] = array('status' => $status, 'status_msg' => $statusMsg);

header("Location: index.php");
exit();
